<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add User</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">
                            Add User
                        </h3>
                    </div>
                    <form name="frmUserAdd" action="lib/add-user.php" method="post" id="frmUserAdd">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Username">Username</label>
                                <input type="text" name="Username" maxlength="60" class="form-control" autocomplete="off" id="username" placeholder="Enter Desired Username">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" name="Password" maxlength="50" class="form-control" autocomplete="off" id="password" placeholder="Enter Password">
                            </div>
                            <div class="form-group">
                                <label for="Confirm Password">Confirm Password</label>
                                <input type="password" name="CPassword" maxlength="50" class="form-control" autocomplete="off" id="cpassword" placeholder="Confirm Above Password">
                            </div>
                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="text" name="email" maxlength="100" class="form-control" id="email" placeholder="Enter Valid Email">
                            </div>
                            <div class="form-group">
                                <label for="First Name">First Name</label>
                                <input type="text" name="fname" maxlength="50" class="form-control" autocomplete="off" id="fname" placeholder="Enter Your First Name">
                            </div>
                            <div class="form-group">
                                <label for="Last Name">Last Name</label>
                                <input type="text" name="lname" maxlength="50" class="form-control" autocomplete="off" id="lname" placeholder="Enter Your Last Name">
                            </div>
                            <div class="form-group">
                                <label for="Status">Status</label><br>
                                <input type="radio" name="status" id="status" value="X05">&nbsp;Student
                                &nbsp;&nbsp;
                                <input type="radio" name="status" id="status" value="X03">&nbsp;Teacher
                            </div>
                        </div>
                        <p class="mohubela">
                            <label for="number">
                                Contact
                                <input type="text" name="contact" maxlength="10" class="input" id="number">
                            </label>
                        </p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div id="ajax-loader" class="pull-right"></div>
                            <label id="error" class="pull-left"></label>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>