<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Pattern Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Add Pattern</h3>
                    </div>
                    <form name="frmPatternAdd" action="lib/add-pattern.php" method="post" id="frmPatternAdd">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Pattern">Pattern Name</label>
                                <input type="text" name="pattern_name" maxlength="50" class="form-control" autocomplete="off" id="pattern_name" placeholder="Enter Desired Pattern Name" required>
                            </div>
                            <div class="form-group">
                                <label for="Marks">Positive Marks</label>
                                <input type="number" name="pos" class="form-control" autocomplete="off" id="pos" placeholder="Positive Marks Per Correct Answer." min="0" max="99" required step="any">
                            </div>
                            <div class="form-group">
                                <label for="Marks">Negative Marks</label>
                                <input type="number" name="neg" class="form-control" autocomplete="off" id="neg" placeholder="Negative Marks Per Wrong Answer." min="0" max="99" required step="any">
                            </div>
                        </div>
                        <p class="mohubela">
                            <label for="number">
                                Contact
                                <input type="text" name="contact" maxlength="10" class="input" id="number">
                            </label>
                        </p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div id="ajax-loader" class="pull-right"></div>
                            <label id="error" class="pull-left"></label>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </section>
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Active Patterns</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="activePattern">
                        </table>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Inactive Patterns</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="inactivePattern">
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>