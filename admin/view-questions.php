<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Question Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <?php
                        if(isset($_GET['set_id'])){
                    ?>
                    <div class="box-header">
                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title" aria-data="<?php echo $_GET['set_id'] ?>">List of Questions</h3>
                    </div>
                    <div class="box-body table-responsive" style="overflow:hidden">
                        <table class="table table-bordered table-hover" id="questions" style="width:100% !important;max-width:100% !important">
                        </table>
                    </div>
                    <?php
                    }
                    else{
                    ?>
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Select A Set</h3>
                    </div>
                    <div class="box-footer">
                        <?php
                            $sql="select id,set_name from oe_sets where status=:status";
                            $stmt=$con->prepare($sql);
                            $stmt->execute(array(
                                'status' => 'X01'
                            ));
                            while($row=$stmt->fetch()){
                        ?>
                            <a href="view-questions?set_id=<?php echo $row['id'] ?>" class="btn btn-primary"><?php echo $row['set_name'] ?></a>
                        <?php
                        }
                        ?>
                    </div>
                    <?php
                    }
                    ?>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>