<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Subject Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Add Subject</h3>
                    </div>
                    <form name="frmSubjectAdd" action="lib/add-subject.php" method="post" id="frmSubjectAdd">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Subject">Subject Name</label>
                                <input type="text" name="subject_name" maxlength="50" class="form-control" autocomplete="off" id="Subject_name" placeholder="Enter Desired Subject Name" required>
                            </div>
                        </div>
                        <p class="mohubela">
                            <label for="number">
                                Contact
                                <input type="text" name="contact" maxlength="10" class="input" id="number">
                            </label>
                        </p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div id="ajax-loader" class="pull-right"></div>
                            <label id="error" class="pull-left"></label>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </section>
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Active Subjects</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="activeSubject">
                        </table>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Inactive Subjects</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="inactiveSubject">
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>