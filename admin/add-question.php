<?php
    require('header.php');
?>
<style>
    .box-body{
        width:50%;
        float:left;
    }
</style>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Question</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Add Question</h3>
                    </div>
                    <form name="frmUserAdd" action="lib/add-question.php" method="post" id="frmQuestionAdd">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Subject">Subject</label>
                                <?php
                                    $sql="select * from oe_subjects where status=:status order by subject_name asc";
                                    $stmt=$con->prepare($sql);
                                    $stmt->execute(array(
                                        'status' => 'X01'
                                    ));
                                ?>
                                <select name="subject_id" class="form-control">
                                    <option value="-1">-- Select The Subject --</option>
                                    <?php while($row=$stmt->fetch()){ ?>
                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['subject_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Directions</label>
                                <textarea id="directions" name="directions" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Question</label>
                                <textarea id="question" name="question" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Option 1</label>
                                <textarea id="option1" name="option1" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Option 2</label>
                                <textarea id="option2" name="option2" class="form-control textarea1"></textarea>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Subject">Set</label>
                                <?php
                                    $sql="select * from oe_sets where status=:status order by set_name asc";
                                    $stmt=$con->prepare($sql);
                                    $stmt->execute(array(
                                        'status' => 'X01'
                                    ));
                                ?>
                                <select name="set_id" class="form-control">
                                    <option value="-1">-- Select The Set --</option>
                                    <?php while($row=$stmt->fetch()){ ?>
                                        <option value="<?php echo $row['id'] ?>"><?php echo $row['set_name'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Option 3</label>
                                <textarea id="option3" name="option3" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Option 4</label>
                                <textarea id="option4" name="option4" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="Questions">Option 5</label>
                                <textarea id="option5" name="option5" class="form-control textarea1"></textarea>
                            </div>
                            <div class="form-group" style="margin-top:10px">
                                <label for="Answers">Correct Answer</label><br>
                                <input type="radio" name="ans" value="1">&nbsp;&nbsp;Option 1&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="ans" value="2">&nbsp;&nbsp;Option 2&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="ans" value="3">&nbsp;&nbsp;Option 3&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="ans" value="4">&nbsp;&nbsp;Option 4&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" name="ans" value="5">&nbsp;&nbsp;Option 5
                            </div>
                        </div>
                        <p class="mohubela">
                            <label for="number">
                                Contact
                                <input type="text" name="contact" maxlength="10" class="input" id="number">
                            </label>
                        </p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div id="ajax-loader" class="pull-right"></div>
                            <label id="error" class="pull-left"></label>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>