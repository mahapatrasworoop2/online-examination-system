<?php
    require('../../lib/functions.php');
    $con=dbConnect();
    if(isset($_POST['status']))
    {
        $status = $_POST['status'];
    }
    else{
        echo "Invalid Status";
        die();
    }
    if($status == 'active'){
        $status_code = 'X01';
        $action[0] = 'reject';
        $action[1] = 'edit';
    }
    else if($status == 'inactive'){
        $status_code = 'X00';
        $action[0] = 'accept';
        $action[1] = 'delete';
    }
    $sql="select * from oe_subjects where status=:status order by id asc";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => $status_code
    ));
?>
<thead>
    <tr>
        <tr>
            <th style="width: 20px">#</th>
            <th>Subject Name</th>
            <th style="width:70px">Action</th>
        </tr>
    </tr>
</thead>
<tbody>
<?php $i=0;while($row=$stmt->fetch()){$i++; ?>
    <tr>
        <td>
            <?php echo $i ?>
        </td>
        <td>
            <p class="value_hide value_hide<?php echo $row['id'] ?>"><?php echo $row['subject_name']; ?></p>
            <input type="text" name="subject_name<?php echo $row['id'] ?>" maxlength="50" class="input_hide input_hide<?php echo $row['id'] ?>" autocomplete="off" id="subject_name" placeholder="Enter Desired Subject Name" required value="<?php echo $row['subject_name']; ?>">
        </td>
        <td align="center">
            <?php
                $space = 0;
                foreach ($action as $key => $value) {
                    $space++;
                    if($space>1)
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo action_link($value,$row['id'],'Subject');
                }
            ?>
        </td>
    </tr>
<?php } ?>
</tbody>
<tfoot>
    <tr>
        <tr>
            <th style="width: 20px">#</th>
            <th>Subject Name</th>
            <th style="width:70px">Action</th>
        </tr>
    </tr>
</tfoot>