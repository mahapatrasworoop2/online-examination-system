<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$honeypot = $_POST['contact'];
	$pattern_name = $_POST['pattern_name'];
	$pos = $_POST['pos'];
	$neg = $_POST['neg'];
	$validate->validate_honeypot($honeypot);
	if($pattern_name==""){
		echo "Invalid Pattern Name";
		die();
	}
	else if($pos==""){
		echo "Invalid Positive Marks";
		die();
	}
	else if($neg==""){
		echo "Invalid Negative Marks";
		die();
	}
	$con=dbConnect();
		try{
			$sql="insert into oe_patterns(pattern_name,pos,neg,status) values(:pattern_name,:pos,:neg,:status)";
			$stmt=$con->prepare($sql);
			$stmt->execute(array(
				'pattern_name' => $pattern_name,
				'pos' => $pos,
				'neg' => $neg,
				'status' => 'X01'
			));
			echo 1;
		}
		catch(PDOException $error) {
			echo DBERROR . $error->getMessage();
			die();
		}
	die();
?>