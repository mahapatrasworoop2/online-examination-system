<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	if(isset($_GET)){
		$con=dbConnect();
		try {
			$timer = $_GET['timer']*60;
			$sql="update oe_sets set set_name=:set_name,timer=:timer where id=:id";
			$stmt=$con->prepare($sql);
			$stmt->execute(array(
                'id' => $_GET['id'],
                'set_name' => $_GET['set_name'],
                'timer' => $timer
            ));
            echo 'Set';
            die();
		}
		catch(PDOException $error) {
			echo 'Sorry! The Program Got An Error : ' . $error->getMessage();
			die();
		}
	}
?>