<?php
    require('../../lib/functions.php');
    $con=dbConnect();
    if(isset($_POST['status']))
    {
        $status = $_POST['status'];
    }
    else{
        echo "Invalid Status";
        die();
    }
    if($status == 'active'){
        $status_code = 'X01';
        $action[0] = 'delete';
    }
    $set_id = $_POST['set_id'];
    $sql="select q.*,b.subject_name,b.id as bid,t.set_name,t.id as tid from oe_questions q,oe_sets t,oe_subjects b where q.status=:status and q.set_id=t.id and q.subject_id=b.id and q.set_id=:set_id order by q.id";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => $status_code,
        'set_id' => $set_id
    ));
?>
<thead>
    <tr>
        <th style="width: 20px">#</th>
        <th>Subject</th>
        <th>Directions</th>
        <th>Question</th>
        <th>Option 1</th>
        <th>Option 2</th>
        <th>Option 3</th>
        <th>Option 4</th>
        <th>Option 5</th>
        <th>Answer</th>
        <th style="width:30px">Action</th>
    </tr>
</thead>
<tbody>
<?php 
    $i=0;
    while($row=$stmt->fetch()){
        $i++;
        $directions = $row['directions'];
        if(strlen($directions)>100){
            $sub_directions = substr($directions, 0, 100);
            $sub_directions = $sub_directions.'...';
        }
        else{
            $sub_directions = $directions;
        }
?>
    <tr>
        <td>
            <?php echo $i ?>
        </td>
        <td>
            <span><?php echo $row['subject_name']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="directions" class="input_hide" id="directions<?php echo $row['id'] ?>" placeholder="Directions"><?php echo $row['directions'] ?></textarea>
            <span class="holder_tag" aria-data="directions"><?php echo $sub_directions; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="question" class="input_hide" id="question<?php echo $row['id'] ?>" placeholder="Question"><?php echo $row['question'] ?></textarea>
            <span class="holder_tag" aria-data="question"><?php echo $row['question']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="option1" class="input_hide" id="option1<?php echo $row['id'] ?>" placeholder="Option One"><?php echo $row['option1'] ?></textarea>
            <span class="holder_tag" aria-data="option1"><?php echo $row['option1']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="option2" class="input_hide" id="option2<?php echo $row['id'] ?>" placeholder="Option Two"><?php echo $row['option2'] ?></textarea>
            <span class="holder_tag" aria-data="option2"><?php echo $row['option2']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="option3" class="input_hide" id="option3<?php echo $row['id'] ?>" placeholder="Option Three"><?php echo $row['option3'] ?></textarea>
            <span class="holder_tag" aria-data="option3"><?php echo $row['option3']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="option4" class="input_hide" id="option4<?php echo $row['id'] ?>" placeholder="Option Four"><?php echo $row['option4'] ?></textarea>
            <span class="holder_tag" aria-data="option4"><?php echo $row['option4']; ?></span>
        </td>
        <td class="edit" aria-data="<?php echo $row['id'] ?>">
            <textarea name="option5" class="input_hide" id="option5<?php echo $row['id'] ?>" placeholder="Option Five"><?php echo $row['option5'] ?></textarea>
            <span class="holder_tag" aria-data="option5"><?php echo $row['option5']; ?></span>
        </td>
        <td class="editAnswer" aria-data="<?php echo $row['id'] ?>">
            <select name="ans" class="input_hide">
                <option value="<?php echo $row['ans'] ?>"><?php echo $row['ans'] ?></option>
                <?php 
                for($j=1;$j<5;$j++){
                    if($j!=$row['ans']){
                ?>
                <option value="<?php echo $j ?>"><?php echo $j ?></option>
                <?php
                    }
                }
                ?>
            </select>
            <span class="holder_tag" id="answer<?php echo $row['id'] ?>"><?php echo $row['ans']; ?></span>
        </td>
        <td align="center" data-id='<?php echo $set_id ?>'>
            <?php
                $space = 0;
                foreach ($action as $key => $value) {
                    $space++;
                    if($space>1)
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo action_link($value,$row['id'],'Question');
                }
            ?>
        </td>
    </tr>
<?php } ?>
</tbody>
<tfoot>
    <tr>
        <th style="width: 20px">#</th>
        <th>Subject</th>
        <th>Directions</th>
        <th>Question</th>
        <th>Option 1</th>
        <th>Option 2</th>
        <th>Option 3</th>
        <th>Option 4</th>
        <th>Option 5</th>
        <th>Answer</th>
        <th style="width:30px">Action</th>
    </tr>
</tfoot>