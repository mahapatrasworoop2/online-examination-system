<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$directions = $_POST['directions'];
	$question = $_POST['question'];
	$option1 = $_POST['option1'];
	$option2 = $_POST['option2'];
	$option3 = $_POST['option3'];
	$option4 = $_POST['option4'];
	$option5 = $_POST['option5'];
	$id = $_POST['id'];
	$con=dbConnect();
	try{
		$sql="update oe_questions set directions=:directions,question=:question,option1=:option1,option2=:option2,option3=:option3,option4=:option4,option5=:option5 where id=:id";
		$stmt=$con->prepare($sql);
		$stmt->execute(array(
			'id' => $id,
			'directions' => $directions,
			'question' => $question,
			'option1' => $option1,
			'option2' => $option2,
			'option3' => $option3,
			'option4' => $option4,
			'option5' => $option5
		));
		echo 1;
	}
	catch(PDOException $error) {
		echo DBERROR . $error->getMessage();
		die();
	}
	die();
?>