<?php
    require('../../lib/functions.php');
    $con=dbConnect();
    if(isset($_POST['status']))
    {
        $status = $_POST['status'];
    }
    else{
        echo "Invalid Status";
        die();
    }
?>
<thead>
    <tr>
        <th style="width: 20px">#</th>
        <th>Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Registered On</th>
        <th>Profile</th>
        <th style="width:70px">Action</th>
    </tr>
</thead>
<?php
    if($status == 'verified'){
        $status_code = 'X05';
        $action[0] = 'reject';
    }
    else if($status == 'unverified'){
        $status_code = 'X00';
        $action[0] = 'accept';
        $action[1] = 'reject';
    }
    else if($status == 'teacher'){
        $status_code = 'X03';
        $action[0] = 'delete';
    }
    else if($status == 'rejected'){
        $status_code = 'X01';
        $action[0] = 'accept';
        $action[1] = 'delete';
    }
    $sql="select * from oe_users where user_status=:status order by user_registered asc";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => $status_code
    ));
?>
<tbody>
    <?php $i=0;while($row=$stmt->fetch()){$i++; ?>
        <tr class="tr">
            <td><?php echo $i ?></td>
            <td><?php echo $row['user_fname'].' '.$row['user_lname']; ?></td>
            <td><?php echo $row['user_login']; ?></td>
            <td><?php echo $row['user_email']; ?></td>
            <td><?php echo date("d/m/Y, H:i:s", strtotime($row['user_registered']));?></td>
                <?php
                    if($row['user_pic']==""){
                        $img='../images/users/user.png';
                ?>
                <?php
                    }
                    else{
                        $img="../images/users/".$row['user_pic'];
                    }
                ?>
            <td><img src="<?php echo $img ?>" alt="User Image" width="20"></td>
            <td align="center">
                <?php
                    $space = 0;
                    foreach ($action as $key => $value) {
                        $space++;
                        if($space>1)
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                        echo action_link($value,$row['user_login'],'User');
                    }
                ?>
            </td>
        </tr>
    <?php } ?>
</tbody>
<tfoot>
    <tr>
        <th style="width: 20px">#</th>
        <th>Name</th>
        <th>Username</th>
        <th>Email</th>
        <th>Registered On</th>
        <th>Profile</th>
        <th style="width:70px">Action</th>
    </tr>
</tfoot>
