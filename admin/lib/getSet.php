<?php
    require('../../lib/functions.php');
    $con=dbConnect();
    if(isset($_POST['status']))
    {
        $status = $_POST['status'];
    }
    else{
        echo "Invalid Status";
        die();
    }
    if($status == 'active'){
        $status_code = 'X01';
        $action[0] = 'reject';
        $action[1] = 'edit';
        $action[3] = 'print';
    }
    else if($status == 'inactive'){
        $status_code = 'X00';
        $action[0] = 'accept';
        $action[1] = 'delete';
    }
    $sql="select s.*,p.pattern_name from oe_sets s, oe_patterns p where s.status=:status and s.pattern_id=p.id order by s.id asc";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => $status_code
    ));
?>
<thead>
    <tr>
        <th style="width: 20px">#</th>
        <th>Set Name</th>
        <th>Time</th>
        <th>Pattern</th>
        <th>Total Questions</th>
        <th style="width:90px">Action</th>
    </tr>
</thead>
<tbody>
<?php $i=0;while($row=$stmt->fetch()){$i++;$sql="select * from oe_questions where set_id=:set_id and status='X01'";$stmt1=$con->prepare($sql);$stmt1->execute(array('set_id' => $row['id']));$questions=$stmt1->rowCount();?>
    <tr>
        <td>
            <?php echo $i ?>
        </td>
        <td>
            <p class="value_hide value_hide<?php echo $row['id'] ?>"><?php echo $row['set_name']; ?></p>
            <input type="text" name="set_name<?php echo $row['id'] ?>" maxlength="50" class="input_hide input_hide<?php echo $row['id'] ?>" autocomplete="off" id="set_name" placeholder="Enter Desired Set Name" required value="<?php echo $row['set_name']; ?>">
        </td>
        <td>
            <p class="value_hide value_hide<?php echo $row['id'] ?>"><?php echo $row['timer']/60; ?></p>
            <input type="text" name="timer<?php echo $row['id'] ?>" maxlength="50" class="input_hide input_hide<?php echo $row['id'] ?>" autocomplete="off" id="timer" placeholder="Enter Desired Time (in Minutes)" required value="<?php echo $row['timer']/60; ?>">
        </td>
        <td><?php echo $row['pattern_name']; ?></td>
        <td>
            <?php echo $questions ?>
        </td>
        <td align="center">
            <?php
                $space = 0;
                foreach ($action as $key => $value) {
                    $space++;
                    if($space>1)
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;";
                    echo action_link($value,$row['id'],'Set');
                }
            ?>
        </td>
    </tr>
<?php } ?>
</tbody>
<tfoot>
    <tr>
        <th style="width: 20px">#</th>
        <th>Set Name</th>
        <th>Time</th>
        <th>Pattern</th>
        <th>Total Questions</th>
        <th style="width:90px">Action</th>
    </tr>
</tfoot>