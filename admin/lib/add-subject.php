<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$honeypot = $_POST['contact'];
	$subject_name = $_POST['subject_name'];
	$validate->validate_honeypot($honeypot);
	if($subject_name==""){
		echo "Invalid Subject Name";
		die();
	}
	$con=dbConnect();
		try{
			$sql="insert into oe_subjects(subject_name,status) values(:subject_name,:status)";
			$stmt=$con->prepare($sql);
			$stmt->execute(array(
				'subject_name' => $subject_name,
				'status' => 'X01'
			));
			echo 1;
		}
		catch(PDOException $error) {
			echo DBERROR . $error->getMessage();
			die();
		}
	die();
?>