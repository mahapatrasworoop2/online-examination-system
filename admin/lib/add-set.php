<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$honeypot = $_POST['contact'];
	$set_name = $_POST['set_name'];
	$timer = $_POST['timer']*60;
	$pattern_id = $_POST['pattern_id'];
	$validate->validate_honeypot($honeypot);
	if($set_name==""){
		echo "Invalid Set Name";
		die();
	}
	else if($timer==""){
		echo "Invalid Timer Value";
		die();
	}
	$con=dbConnect();
		try{
			$sql="insert into oe_sets(set_name,timer,pattern_id,status) values(:set_name,:timer,:pattern_id,:status)";
			$stmt=$con->prepare($sql);
			$stmt->execute(array(
				'set_name' => $set_name,
				'timer' => $timer,
				'pattern_id' => $pattern_id,
				'status' => 'X01'
			));
			echo 1;
		}
		catch(PDOException $error) {
			echo DBERROR . $error->getMessage();
			die();
		}
	die();
?>