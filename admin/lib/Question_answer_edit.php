<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$id = $_POST['id'];
	$ans = $_POST['ans'];
	$con=dbConnect();
	try{
		$sql="update oe_questions set ans=:ans where id=:id";
		$stmt=$con->prepare($sql);
		$stmt->execute(array(
			'id' => $id,
			'ans' => $ans
		));
		echo 1;
	}
	catch(PDOException $error) {
		echo DBERROR . $error->getMessage();
		die();
	}
	die();
?>