<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	if(isset($_GET)){
		$con=dbConnect();
		try {
			$sql="update oe_patterns set pattern_name=:pattern_name,pos=:pos,neg=:neg where id=:id";
			$stmt=$con->prepare($sql);
			$stmt->execute(array(
                'id' => $_GET['id'],
                'pattern_name' => $_GET['pattern_name'],
                'pos' => $_GET['pos'],
                'neg' => $_GET['neg']
            ));
            echo 'Pattern';
            die();
		}
		catch(PDOException $error) {
			echo 'Sorry! The Program Got An Error : ' . $error->getMessage();
			die();
		}
	}
?>