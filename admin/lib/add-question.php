<?php
	require('../../lib/functions.php');
	$validate=new Validators();
	$validate->validate_admin('../../teacher/');
	$honeypot = $_POST['contact'];
	$validate->validate_honeypot($honeypot);
	$directions = $_POST['directions'];
	$question = $_POST['question'];
	$option1 = $_POST['option1'];
	$option2 = $_POST['option2'];
	$option3 = $_POST['option3'];
	$option4 = $_POST['option4'];
	$option5 = $_POST['option5'];
	$set_id = $_POST['set_id'];
	$subject_id = $_POST['subject_id'];
	if($question==""){
		echo "Please Enter The Question";
		die();
	}
	else if($option1==""){
		echo "Please Enter The Option 1";
		die();
	}
	else if($option2==""){
		echo "Please Enter The Option 2";
		die();
	}
	else if($option3==""){
		echo "Please Enter The Option 3";
		die();
	}
	else if($option4==""){
		echo "Please Enter The Option 4";
		die();
	}
	else if($subject_id=="-1"){
		echo "Please Select The Subject";
		die();
	}
	else if($set_id=="-1"){
		echo "Please Select The Set";
		die();
	}
	if(isset($_POST['ans'])){
		$ans = $_POST['ans'];
	}
	else{
		echo "Please Select The Correct Answer";
		die();
	}
	$con=dbConnect();
	try{
		$sql="insert into oe_questions(directions,question,option1,option2,option3,option4,option5,ans,set_id,subject_id,status) values(:directions,:question,:option1,:option2,:option3,:option4,:option5,:ans,:set_id,:subject_id,:status)";
		$stmt=$con->prepare($sql);
		$stmt->execute(array(
			'directions' => $directions,
			'question' => $question,
			'option1' => $option1,
			'option2' => $option2,
			'option3' => $option3,
			'option4' => $option4,
			'option5' => $option5,
			'ans' => $ans,
			'set_id' => $set_id,
			'subject_id' => $subject_id,
			'status' => 'X01'
		));
		echo 1;
	}
	catch(PDOException $error) {
		echo DBERROR . $error->getMessage();
		die();
	}
	die();
?>