<?php
    require('../../lib/functions.php');
    $con=dbConnect();
    if(isset($_POST['status']))
    {
        $status = $_POST['status'];
    }
    else{
        echo "Invalid Status";
        die();
    }
    if($status == 'active'){
        $status_code = 'X01';
        $action[0] = 'reject';
    }
    else if($status == 'inactive'){
        $status_code = 'X00';
        $action[0] = 'accept';
        $action[1] = 'delete';
    }
    $sql="select q.*,b.subject_name,b.id as bid,t.set_name,t.id as tid from oe_questions q,oe_sets t,oe_subjects b where q.status=:status and q.set_id=t.id and q.subject_id=b.id and q.set_id=:set_id order by q.id";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => $status_code,
        'set_id' => $_POST['set_id']
    ));
?>
<ol>
<?php
    $subject_name_old = "";
    $directions_old = "";
    while($row=$stmt->fetch()){
        $directions = $row['directions'];
        $subject_name = $row['subject_name'];
        if($subject_name!=$subject_name_old){
            $subject_name_old=$subject_name;
?>
<p style="text-align:center;font-weight:bold"><?php echo $subject_name ?></p>
<?php
        }      
?>
<li>
    <ul style="list-style:none;">
        <?php 
            if($directions!=$directions_old) {
                $directions_old = $directions;
                if($directions_old!=""){
        ?>
        <li><strong>Directions : </strong><?php echo $row['directions'] ?></li>
        <?php
                }
            }
        ?>
        <li><strong>Question : </strong><?php echo $row['question'] ?></li>
        <li>
            <span class="print_options"><strong>A.</strong> &nbsp;<?php echo strip_tags($row['option1']) ?>&nbsp;</span>
            <span class="print_options"><strong>B.</strong> &nbsp;<?php echo strip_tags($row['option2']) ?>&nbsp;</span>
            <span class="print_options"><strong>C.</strong> &nbsp;<?php echo strip_tags($row['option3']) ?>&nbsp;</span>
            <span class="print_options"><strong>D.</strong> &nbsp;<?php echo strip_tags($row['option4']) ?>&nbsp;</span>
            <span class="print_options"><strong>E.</strong> &nbsp;<?php echo strip_tags($row['option5']) ?></span>
            <div class="clearfix"></div>
        </li>
    </ul>
</li>
<?php } ?>