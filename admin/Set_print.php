<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content">
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <?php
                        if(isset($_GET['key'])){
                    ?>
                    <div class="box-header" style="display:none;">
                        <div class="pull-right box-tools">
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title" aria-data="<?php echo base64_decode($_GET['key']) ?>">Print Set</h3>
                    </div>
                    <div class="box-body" id="print_questions">
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>