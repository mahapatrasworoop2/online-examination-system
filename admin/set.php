<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Set Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Add Set</h3>
                    </div>
                    <form name="frmSetAdd" action="lib/add-set.php" method="post" id="frmSetAdd">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="Set">Set Name</label>
                                <input type="text" name="set_name" maxlength="50" class="form-control" autocomplete="off" id="Set_name" placeholder="Enter Desired Set Name" required>
                            </div>
                            <div class="form-group">
                                <label for="Timer">Time (In Minutes)</label>
                                <input type="number" name="timer" maxlength="10" class="form-control" autocomplete="off" id="timer" placeholder="Total Time Of Examination." min="0" max="1440" required>
                            </div>
                            <div class="form-group">
                                <label for="Pattern">Pattern</label>
                                <select class="form-control" name="pattern_id" id="pattern_id">
                                <?php
                                    $sql="select * from oe_patterns where status=:status order by id asc";
                                    $stmt=$con->prepare($sql);
                                    $stmt->execute(array(
                                        'status' => 'X01'
                                    ));
                                    while($row=$stmt->fetch()){?>
                                    <option value="<?php echo $row['id'] ?>"><?php echo $row['pattern_name'] ?></option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                        <p class="mohubela">
                            <label for="number">
                                Contact
                                <input type="text" name="contact" maxlength="10" class="input" id="number">
                            </label>
                        </p>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            <div id="ajax-loader" class="pull-right"></div>
                            <label id="error" class="pull-left"></label>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </section>
            <section class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Active Sets</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="activeSet">
                        </table>
                    </div>
                </div>
            </section>
        </div>
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Inactive Sets</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="inactiveSet">
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>