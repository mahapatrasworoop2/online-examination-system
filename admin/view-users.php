<?php 
    require('header.php');
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Management</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Newly Registered Students (Unverified)</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="unverified">
                        </table>
                    </div>
                </div>
            </section>
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Verified Students</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="verified">
                            
                        </table>
                    </div>
                </div>
            </section>
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Rejected Students</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="rejected">
                        </table>
                    </div>
                </div>
            </section>
            <section class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="pull-right box-tools">                                        
                            <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Teachers</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <table class="table table-bordered table-hover" id="teachers">
                            
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>