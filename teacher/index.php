<?php 
    require('header.php');
    /* Count New Registrations */
    $sql="select * from oe_users where user_status=:status";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => 'X00'
    ));
    $new_reg_count=$stmt->rowCount();
    /* Count Rejected Users */
    $sql="select * from oe_users where user_status=:status";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => 'X01'
    ));
    $rejected_reg_counts=$stmt->rowCount();
    /* Count Deleted Users */
    $sql="select * from oe_users where user_status=:status";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => 'X02'
    ));
    $deleted_reg_counts=$stmt->rowCount();
    /* Teacher Counts */
    $sql="select * from oe_users where user_status=:status";
    $stmt=$con->prepare($sql);
    $stmt->execute(array(
        'status' => 'X03'
    ));
    $teacher_counts=$stmt->rowCount();
?>
<aside class="right-side">
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="../admin/"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $new_reg_count; ?></h3>
                        <p>New Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-contacts"></i>
                    </div>
                    <a href="view-users" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $rejected_reg_counts; ?></h3>
                        <p>Rejected Users</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-contacts"></i>
                    </div>
                    <a href="view-users" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $deleted_reg_counts; ?></h3>
                        <p>Deleted Users</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-contacts"></i>
                    </div>
                    <a href="view-users" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo $teacher_counts; ?></h3>
                        <p>Teachers</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-android-contacts"></i>
                    </div>
                    <a href="view-users" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>