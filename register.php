<?php
require('lib/functions.php');
if (isset($_SESSION['username'])) {
    header('location:admin/');
    die();
}
?>
<!DOCTYPE html>
<html>
    <head>
    	<title><?php echo TITLE ?></title>
    	<link href="css/login.css" rel="stylesheet" type="text/css">
    	<link rel="shortcut icon" href="images/icons/favicon.ico?v=2">
    </head>
    <body>
    	<noscript>
    		<div>Please Enable JavaScript Or Get A Better Browser To Use This Site</div>
    	</noscript>
    	<div class="box-1">
    		<h1><a href="<?php echo URL?>" title="<?php echo TITLE ?>"><?php echo TITLE ?></a></h1>
    		<p id="error"></p>
    		<form name="frmLogin" action="lib/register.php" method="post" id="frmReg">
                <p>
                    <label for="login_username">
                        Username
                        <input type="text" name="Username" size="20" maxlength="60" class="input" autocomplete="off" id="username">
                    </label>
                </p>
                <p>
                    <label for="login_password">
                        Password
                        <input type="password" name="Password" size="20" maxlength="50" class="input" autocomplete="off" id="password">
                    </label>
                </p>
                <p>
                    <label for="login_password">
                        Confirm Password
                        <input type="password" name="CPassword" size="20" maxlength="50" class="input" autocomplete="off" id="cpassword">
                    </label>
                </p>
                <p>
                    <label for="email">
                        Email
                        <input type="text" name="email" size="20" maxlength="100" class="input" id="email">
                    </label>
                </p>
                <p>
                    <label for="fname">
                        First Name
                        <input type="text" name="fname" size="20" maxlength="50" class="input" autocomplete="off" id="fname">
                    </label>
                </p>
                <p>
                    <label for="lname">
                        Last Name
                        <input type="text" name="lname" size="20" maxlength="50" class="input" autocomplete="off" id="lname">
                    </label>
                </p>
                <p class="mohubela">
                    <label for="number">
                        Contact
                        <input type="text" name="contact" size="20" maxlength="10" class="input" id="number">
                    </label>
                </p>
                <p>
                	<input type="submit" name="submit" value="Register" class="button fright">
                	<div id="ajax-loader" class="fright"></div>
    				<div class="clear"></div>
                </p>
            </form>
            <p class="reg-link"><a href="<?php echo URL ?>/online-examination-system">Login</a> | <a href="forgot">Lost your password?</a> | <a href="verify-email">Verify Email</a></p>
            <p class="reg-link">
                <a href="<?php echo URL; ?>">&#8592; Back to 
                    <?php echo TITLE; ?>
                </a>
            </p>
    	</div>
    </body>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.ui.js"></script>
    <script type="text/javascript" src="js/custom.min.js"></script>
</html>