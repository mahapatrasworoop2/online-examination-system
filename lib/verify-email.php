<?php
	require('functions.php');
	$con=dbConnect();
	$honeypot = $_POST['contact'];
	$username = $_POST['Username'];
	$user_email_activation_key = $_POST['user_email_activation_key'];
	$email = $_POST['email'];
	$validate=new Validators();
	$validate->validate_honeypot($honeypot);
	$validate->validate_username($username);
	$validate->validate_email($email);
	$validate->validate_email_verification($username,$email,$user_email_activation_key);
	die();
?>