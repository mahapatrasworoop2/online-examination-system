<?php
	require('functions.php');
	$con=dbConnect();
	$username = $_POST['Username'];
	$password = $_POST['Password'];
	$honeypot = $_POST['Contact'];
	$validate=new Validators();
	$validate->validate_honeypot($honeypot);
	$validate->validate_username($username);
	$validate->validate_password($password);
	$validate->validate_login($username,$password);
	die();
?>