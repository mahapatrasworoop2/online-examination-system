<?php
	error_reporting(-1);
	session_start();
	date_default_timezone_set('Asia/Calcutta');
	define('TITLE','Code Infi');
	define('URL','http://'.$_SERVER['HTTP_HOST']);
	define('ADMIN','admin@codeinfi.com');
	define('DBHOST','localhost');
    define('DBNAME','personal_onlineexam');
    define('DBUSER','root');
    define('DBPASS','');
    define('DBERROR','Sorry! Database Got An Error : ');
    function dbConnect(){
		try {
	        $con = new PDO('mysql:host='.DBHOST.';dbname='.DBNAME, DBUSER, DBPASS);
	        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $con;
	    }
	    catch(PDOException $error) {
	        echo DBERROR . $error->getMessage();
	    }
	}
	class Generators {
        public function generateRandom($length) {
            return substr(md5(uniqid(rand(), true)), 0, $length);
        }
        public function generateHash($string) {
            if (defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
                $salt = '$2y$11$' . substr(md5(uniqid(rand(), true)), 0, 22);
                return crypt($string, $salt);
            }
            else {
                echo "Sorry! Please Enable BlowFish";
                die();
            }
        }
    }
    function send_email($message,$from,$name,$subject,$to){
        $args = array(
            'key' => 'ZMD-zcp2NN3JbDRw-Oal6w',
            'message' => array(
                "html" => $message,
                "text" => null,
                "from_email" => $from,
                "from_name" => $name,
                "subject" => $subject,
                "to" => array(array('email' => $to)),
                "track_opens" => true,
                "track_clicks" => true,
                "auto_text" => true
            )   
        );
        $curl = curl_init('https://mandrillapp.com/api/1.0/messages/send.json' );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($args));        
        $response = curl_exec($curl);
        $report = json_decode($response, true);
        $email_status=$report[0]['status'];
        curl_close( $curl );
        if($email_status!='sent'){
            return false;
        }
        else{
            return true;
        }
    }
    class Validators {
        private $username_regex = "/^([A-Za-z0-9_\-]{3,60})$/";
        private $password_regex = "/^([A-Za-z0-9!@#$%\^&*()_\-]{6,50})$/";
        private $email_regex = "/^([a-zA-Z0-9_\.\-]+)@([\da-z\.\-]+)\.([a-z\.]{2,100})$/";
        private $name_regex = "/^[A-Za-z]{3,50}$/";
        public function validate_honeypot($honeypot) {
            if($honeypot==="") {
                return true;
            }
            else{
                echo '<strong>Error : </strong>Please Do Not Spam.';
                die();
            }
        }
        public function validate_admin($redirect){
            if(!(isset($_SESSION['status']) and $_SESSION['status']==='X04')){
                echo "<script>window.location.href = \"$redirect\";</script>";
                die();
            }
        }
        public function validate_teacher($redirect){
            if(!(isset($_SESSION['status']) and ($_SESSION['status']==='X03' || $_SESSION['status']==='X04'))){
                echo "<script>window.location.href = \"$redirect\";</script>";
                die();
            }
        }
        public function validate_student($redirect){
            if(!(isset($_SESSION['status']) and ($_SESSION['status']==='X05' || $_SESSION['status']==='X04' || $_SESSION['status']=='X03'))){
                echo "<script>window.location.href = \"$redirect\";</script>";
                die();
            }
        }
        public function validate_username($username) {
            if(!(preg_match($this->username_regex,$username))) {
                echo '<strong>Error : </strong>Sorry! Invalid Username.';
                die();
            }
            else {
                return true;
            }
        }
        public function validate_password($password) {
            if(!(preg_match($this->password_regex,$password))) {
                echo '<strong>Error : </strong>Sorry! Invalid Password.';
                die();
            }
            else {
                return true;
            }
        }
        public function validate_name($name) {
            if(!(preg_match($this->name_regex,$name))) {
                echo '<strong>Error : </strong>Sorry! Invalid First or Last Name.';
                die();
            }
            else {
                return true;
            }
        }
        public function validate_email($email) {
            if(!(preg_match($this->email_regex,$email))) {
                echo '<strong>Error : </strong>Sorry! Invalid Email.';
                die();
            }
            else {
                return true;
            }
        }
        public function exists_email($email){
            $con=dbConnect();
            try {
                $sql="select user_email from oe_users where user_email=:email";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'email' => $email
                ));
                if($stmt->fetch()){
                    echo '<strong>Error : </strong>Sorry! Email Is Already Registered.';
                    die();
                }
                else{
                    return true;
                }
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function exists_username($username){
            $con=dbConnect();
            try {
                $sql="select user_login from oe_users where user_login=:username";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'username' => $username
                ));
                if($stmt->fetch()){
                    echo '<strong>Error : </strong>Sorry! Username Is Already Registered.';
                    die();
                }
                else{
                    return true;
                }
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function validate_login($username,$password) {
            $con=dbConnect();
            try{
                $sql="select * from oe_users where user_login=:username";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'username' => $username
                ));
                if($row=$stmt->fetch()) {
                    if(crypt($password, $row['user_pass']) == $row['user_pass']){
                        $ip=$_SERVER['REMOTE_ADDR'];
                        $dt=date('Y-m-d H:i:s');
                        $sql="insert into oe_login_history(user_login,user_last_login,user_last_login_ip) values(:username,:dt,:ip)";
                        $stmt=$con->prepare($sql);
                        $stmt->execute(array(
                            'username' => $username,
                            'dt' => $dt,
                            'ip' => $ip
                        ));
                        $_SESSION['username']=$username;
                        $_SESSION['status']=$row['user_status'];
                        $_SESSION['fname']=$row['user_fname'];
                        $_SESSION['lname']=$row['user_lname'];
                        $_SESSION['email']=$row['user_email'];
                        $_SESSION['pic']=$row['user_pic'];
                        $_SESSION['registered']=$row['user_registered'];
                        if($_SESSION['status']==='X00') {
                            session_destroy();
                            echo '<strong>Error : </strong>Sorry! Your Account Has Not Yet Been Verified. Please Ask Your Institute Administrator To Verify.';
                            die();
                        }
                        else if($_SESSION['status']==='X01') {
                            session_destroy();
                            echo '<strong>Error : </strong>Sorry! Your Account Has Been Rejected By Administrator. Please Contact Your Institute Administrator For Details.';
                            die();
                        }
                        else if($_SESSION['status']==='X02') {
                            session_destroy();
                            echo '<strong>Error : </strong>Sorry! Your Have Deleted Your Account. Please Contact Your Institute Administrator To Undelete.';
                            die();
                        }
                        else{
                            echo 1;
                        }
                    }
                    else{
                        echo '<strong>Error : </strong>Sorry! Wrong Password.';
                        die();
                    }
                }
                else{
                    echo '<strong>Error : </strong>Sorry! Username Doesn\'t Exist.';
                    die();
                }
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function match_passwords($password,$cpassword) {
            if($password===$cpassword) {
                return true;
            }
            else{
                echo '<strong>Error : </strong>Sorry! Passwords Do Not Match';
                die();
            }
        }
        public function validate_registration($username,$password,$email,$fname,$lname,$status='X00'){
            $con=dbConnect();
            $generate=new Generators();
            $password=$generate->generateHash($password);
            $user_registered=date('Y-m-d H:i:s');
            $user_email_activation_key=$generate->generateRandom(7);
            $mail_message = "Your 7 Letter Verification Key : $user_email_activation_key";
            send_email($mail_message,ADMIN,'Administrator','Email Verification Code',$email);
            $user_email_activation_key=$generate->generateHash($user_email_activation_key);
            try{
                $sql="insert into oe_users(user_login,user_pass,user_email,user_registered,user_email_activation_key,user_status,user_fname,user_lname) values(:username,:password,:email,:user_registered,:user_email_activation_key,:status,:user_fname,:user_lname)";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'username' => $username,
                    'password' => $password,
                    'email' => $email,
                    'user_registered' => $user_registered,
                    'user_email_activation_key' => $user_email_activation_key,
                    'status' => $status,
                    'user_fname' => $fname,
                    'user_lname' => $lname
                ));
                echo 1;
                die();
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function validate_email_verification($username,$email,$user_email_activation_key) {
            $con=dbConnect();
            try{
                $sql="select user_email_activation_key from oe_users where user_login=:username and user_email=:email";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'username' => $username,
                    'email' => $email
                ));
                if($row=$stmt->fetch()) {
                    if(crypt($user_email_activation_key, $row['user_email_activation_key']) == $row['user_email_activation_key']){
                        $sql="update oe_users set user_email_activation_key=:user_email_activation_key where user_login=:username";
                        $stmt=$con->prepare($sql);
                        $stmt->execute(array(
                            'username' => $username,
                            'user_email_activation_key' => ""
                        ));
                        echo 1;
                    }
                    else{
                        echo "<strong>Sorry! Wrong Key.</strong>";
                    }
                }
                else{
                    echo '<strong>Error : </strong>Sorry! Username or Email Doesn\'t Exist.';
                    die();
                }
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function validate_reset_password($username,$email){
            $con=dbConnect();
            try{
                $generate=new Generators();
                $user_pass_reset_key=$generate->generateRandom(7);
                $sql = "select user_login from oe_users where user_email=:email";
                $stmt=$con->prepare($sql);
                $stmt->execute(array(
                    'email' => $email
                ));
                if($row=$stmt->fetch()){
                    if($username===$row['user_login']){
                        $mail_message = "Click On The Following Password Reset Link <a href='".URL."/online-examination-system/lib/forgot?user_pass_reset_key=".base64_encode($user_pass_reset_key)."&user_email=".base64_encode($email)."&user_login=".base64_encode($username)."'>Reset Password</a>";
                        $user_pass_reset_key = $generate->generateHash($user_pass_reset_key);
                        $sql = "update oe_users set user_pass_reset_key=:user_pass_reset_key where user_login=:username and user_email=:email";
                        $stmt=$con->prepare($sql);
                        $stmt->execute(array(
                            'email' => $email,
                            'username' => $username,
                            'user_pass_reset_key' => $user_pass_reset_key
                        ));
                        send_email($mail_message,ADMIN,'Administrator','Password Reset Link',$email);
                        echo 1;
                        die();
                    }
                }
                else{
                    echo '<strong>Sorry! Email Doesn\'t Exist</strong>';
                    die();
                }
            }
            catch(PDOException $error) {
                echo DBERROR . $error->getMessage();
                die();
            }
        }
        public function validate_reset_password_send_email($get){
            $con=dbConnect();
            $username=base64_decode($get['user_login']);
            $email=base64_decode($get['user_email']);
            $user_pass_reset_key=base64_decode($get['user_pass_reset_key']);
            $generate=new Generators();
            $password=$generate->generateRandom(10);
            $mail_message = "Your Password Has Been Reset. Your New Password Is ".$password;
            $password = $generate->generateHash($password);
            $sql="select user_pass_reset_key from oe_users where user_login=:username and user_email=:email";
            $stmt=$con->prepare($sql);
            $stmt->execute(array(
                'username' => $username,
                'email' => $email
            ));
            if($row=$stmt->fetch()) {
                if(crypt($user_pass_reset_key, $row['user_pass_reset_key']) == $row['user_pass_reset_key']){
                    $sql="update oe_users set user_pass_reset_key=:user_pass_reset_key,user_pass=:user_pass where user_login=:username";
                    $stmt=$con->prepare($sql);
                    $stmt->execute(array(
                        'username' => $username,
                        'user_pass_reset_key' => "",
                        'user_pass' => $password
                    ));
                    send_email($mail_message,ADMIN,'Administrator','Password Reset Successfully',$email);
                    header('location:../index');
                    die();
                }
                else{
                    header('location:../forgot');
                    die();
                }
            }
        }
    }
    function reset_session(){
        $keep = array('username','status','fname','lname','email','pic','registered');
        foreach ($_SESSION as $key => $value) {
            if(!in_array($key, $keep)){
                unset($_SESSION[$key]);
            }
        }
        if(isset($_COOKIE['timer'])){
            echo "<script>document.cookie = 'timer=deleted'</script>";
        }
    }
    function action_link($action,$key,$table){
        switch ($action) {
            case 'delete':
                return "<a href='lib/".$table."_delete.php?key=".base64_encode($key)."' title='Delete ".$table."' class='getCall' data-toggle='tooltip'><i class='fa fa-trash-o fa-lg' style='color:#f56954;'></i></a>";
                break;
            case 'reject':
                return "<a href='lib/".$table."_reject.php?key=".base64_encode($key)."' title='Reject ".$table."' class='getCall'><i class='fa fa-times fa-lg' style='color:#f56954;'></i></a>";
                break;
            case 'accept':
                return "<a href='lib/".$table."_accept.php?key=".base64_encode($key)."' title='Accept ".$table."' class='getCall'><i class='fa fa-check fa-lg' style='color:#00a65a;'></i></a>";
                break;
            case 'edit':
                return "<a href='$key' title='Edit ".$table."' class='".$table."EditCall'><i class='fa fa-edit fa-lg' style='color:#3c8dbc;margin-top:2px'></i></a>";
                break;
            case 'print':
                return "<a href='".$table."_print?key=".base64_encode($key)."' title='Print ".$table."'><i class='fa fa-print fa-lg' style='color:#00a65a;margin-top:2px'></i></a>";
                break;
            default:
                break;
        }
    }
?>
