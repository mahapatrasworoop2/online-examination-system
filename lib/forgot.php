<?php
	require('functions.php');
	$con=dbConnect();
	$honeypot = $_POST['contact'];
	$username = $_POST['Username'];
	$email = $_POST['email'];
	$validate=new Validators();
	$validate->validate_honeypot($honeypot);
	$validate->validate_username($username);
	$validate->validate_email($email);
	$validate->validate_reset_password($username,$email);
	die();
?>