<?php 
    require('header.php');
    if(isset($_POST['policy']) || isset($_SESSION['policy'])){
        $_SESSION['policy']=1;
    }
    else{
        echo "<script>window.location.href='sets'</script>";
        die();
    }
    if(isset($_COOKIE['timer']) and $_COOKIE['timer']!='deleted'){
        $_SESSION['timer']=$_COOKIE['timer'];
        unset($_COOKIE['timer']);
        if($_SESSION['timer']<1){
            echo "<script>window.location.href=\"finish\";</script>";
            die();
        }
    }
    if(isset($_GET['id']) && isset($_GET['sid'])){
        $id=$_GET['id'];
        $prev_id=$id;
        $sid=$_GET['sid'];
    }
    else{
        echo "<script>alert('You\'re Being Dissallowed To Give Exam Due To Malpractice');window.location.href='sets'</script>";
        die();
    }
    if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_POST['action'])){
        $var = 'q'.$_SESSION['subject_id'][$sid];
        $id-=2;
        if($_POST['action']==="sn"){
            if(isset($_POST['ans'])){
                $_SESSION[$var]['ans'][$id]=$_POST['ans'];// Answered
            }
            else{
                $_SESSION[$var]['ans'][$id]=6;//Skipped 
            }
            $_SESSION[$var]['mfr'][$id]=0;// Marked For Review
        }
        else if($_POST['action']==='mfr'){
            if(isset($_POST['ans'])){
                $_SESSION[$var]['ans'][$id]=$_POST['ans'];
                $_SESSION[$var]['mfr'][$id]=1;// Marked For Review
            }
            else{
                $_SESSION[$var]['ans'][$id]=6;//Skipped
                $_SESSION[$var]['mfr'][$id]=0;// Marked For Review
            }   
        }
        else if($_POST['action']==='skip'){
            $_SESSION[$var]['ans'][$id]=6;//Skipped
            $_SESSION[$var]['mfr'][$id]=0;// Marked For Review
        }
        $id+=2;
    }
    $temp_var = 'q'.$_SESSION['subject_id'][$sid];
    if(!isset($_SESSION[$temp_var]['qid'][$id-1])){
        $sid++;
        if($sid>=count($_SESSION['subject_id'])){
            echo "<script>window.location.href='finish'</script>";
            die();
        }
        else{
            echo "<script>window.location.href='exam?id=1&sid=".$sid."'</script>";
            die();   
        }
    }
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
            <div id="timer" style="text-align:center;font-weight:bold;">
                Time Left : <span id="countdown-1"><?php echo $_SESSION['timer']; ?></span>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($sid==$key){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <li class="<?php echo $class;?>"><a href="#<?php echo str_replace(" ","",$value); ?>" data-toggle="tab"><?php echo $_SESSION['subject_name'][$key]; ?></a></li>
                    <?php
                        }
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($sid==$key){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <div class="tab-pane <?php echo $class; ?>" id="<?php echo str_replace(" ","",$value);; ?>">
                        <section class="col-lg-9" style="min-height:500px">
                            <div class="box box-primary">
                                <?php
                                    $var = 'q'.$value;
                                    if(!isset($_SESSION[$var]['qid'][$id-1])){
                                        $id=1;
                                    }
                                    $qid=$_SESSION[$var]['qid'][$id-1];
                                    if($class=='active'){
                                        if($_SESSION[$var]['ans'][$id-1]==0){
                                            $_SESSION[$var]['ans'][$id-1]=-1;
                                        }
                                    }
                                    $sql="select * from oe_questions where id=:id";
                                    try{
                                        $stmt=$con->prepare($sql);
                                        $stmt->execute(array(
                                            'id' => $qid
                                        ));
                                        if($row=$stmt->fetch()){
                                            $question=trim($row['question']);
                                            $option1=trim($row['option1']);
                                            $option2=trim($row['option2']);
                                            $option3=trim($row['option3']);
                                            $option4=trim($row['option4']);
                                            $option5=trim($row['option5']);
                                            if(trim($row['directions'])==""){
                                                for($dir=$qid;$dir>=$_SESSION[$var]['qid'][0];$dir--){
                                                    $dir_sql="select directions from oe_questions where id=:id";
                                                    $dir_stmt=$con->prepare($sql);
                                                    $dir_stmt->execute(array(
                                                        'id' => $dir
                                                    ));
                                                    if($dir_row=$dir_stmt->fetch()){
                                                        if(trim($dir_row['directions'])!=""){
                                                            $directions=trim($dir_row['directions']);
                                                            break;
                                                        }
                                                        else{
                                                            $directions="";
                                                        }
                                                    }
                                                }
                                                unset($dir_sql);unset($dir_row);unset($dir_stmt);
                                            }
                                            else{
                                                $directions=trim($row['directions']);
                                            }
                                        }
                                    }
                                    catch(PDOException $error) {
                                        echo DBERROR . $error->getMessage();
                                        die();
                                    }
                                ?>
                                <form name="frmExam" action="exam?id=<?php echo $id+1 ?>&amp;sid=<?php echo $key ?>" method="post" id="frmExam<?php echo $key ?>">
                                    <div class="box-body">
                                        <table class="table table-bordered table-hover">
                                            <tr>
                                                <td colspan="2" style="color:#F00;text-align:center;font-weight:bold;">Please Ignore Directions If Not Relevant To Question</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><strong><?php echo $directions; ?></strong></td>
                                            </tr>
                                            <tr>
                                                <td style="width:50px"><strong><?php echo $_SESSION[$var]['qno'][$id-1] ?>.</strong></td>
                                                <td><?php echo $question; ?></td>
                                            </tr>
                                            <tr>
                                                <td>A. <input type="radio" class="form-control" value="1" name="ans" <?php if($_SESSION[$var]['ans'][$id-1]==1){echo "checked";} ?>></td>
                                                <td class="option"><?php echo $option1; ?></td>
                                            </tr>
                                            <tr>
                                                <td>B. <input type="radio" class="form-control" value="2" name="ans" <?php if($_SESSION[$var]['ans'][$id-1]==2){echo "checked";} ?>></td>
                                                <td class="option"><?php echo $option2; ?></td>
                                            </tr>
                                            <tr>
                                                <td>C. <input type="radio" class="form-control" value="3" name="ans" <?php if($_SESSION[$var]['ans'][$id-1]==3){echo "checked";} ?>></td>
                                                <td class="option"><?php echo $option3; ?></td>
                                            </tr>
                                            <tr>
                                                <td>D. <input type="radio" class="form-control" value="4" name="ans" <?php if($_SESSION[$var]['ans'][$id-1]==4){echo "checked";} ?>></td>
                                                <td class="option"><?php echo $option4; ?></td>
                                            </tr>
                                            <tr>
                                                <td>E. <input type="radio" class="form-control" value="5" name="ans" <?php if($_SESSION[$var]['ans'][$id-1]==5){echo "checked";} ?>></td>
                                                <td class="option"><?php echo $option5; ?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="box-footer">
                                        <p align="center">
                                            <input type="hidden" value="sn" name="action" id="action<?php echo $key ?>">
                                            <button type="button" class="btn btn-primary mfr" aria-data='<?php echo $key ?>'>Mark For Review</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary skip" aria-data='<?php echo $key ?>'>Skip</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary reset">Reset</button>&nbsp;&nbsp;
                                            <button type="button" class="btn btn-primary sn" aria-data='<?php echo $key ?>'>Save &amp; Next</button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </section>
                        <section class="col-lg-3">
                            <div class="box box-danger">
                                <div class="box-body">
                                    <div id="jump" style="overflow-y:scroll">
                                    <?php
                                        $count=count($_SESSION[$var]['qid']);
                                        for($i=1;$i<=$count;$i++){
                                            $j=$i-1;
                                            if($_SESSION[$var]['ans'][$j]==0){
                                                $class="white";//Not Attempted
                                            }
                                            if($_SESSION[$var]['ans'][$j]==6 || $_SESSION[$var]['ans'][$j]==-1){
                                                $class="red";//Skipped
                                            }
                                            if($_SESSION[$var]['ans'][$j]!=-1 && $_SESSION[$var]['ans'][$j]!=6 &&$_SESSION[$var]['ans'][$j]!=0){
                                                $class="green";// Answered
                                            }
                                            if($_SESSION[$var]['mfr'][$j]==1 && $_SESSION[$var]['ans'][$j]!=6 && $_SESSION[$var]['ans'][$j]!=0){
                                                $class="purple";//Marked For Review
                                            }
                                    ?>
                                        <a href="exam?id=<?php echo $i; ?>&amp;sid=<?php echo $key ?>"><div id="qbox" class="<?php echo $class ?>"><?php echo $_SESSION[$var]['qno'][$i-1] ?></div></a>
                                    <?php 
                                        }
                                    ?>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary pull-left saq">See All Questions</button>
                                    <button type="button" class="btn btn-primary pull-right inst">View Instructions</button>
                                    <div class="clearfix"></div>
                                    <p align="center" style="margin-top:10px"><button type="button" class="btn btn-primary finish">Finish</button></p>
                                </div>
                            </div>
                        </section>
                    </div>
                    <?php
                            $id=$prev_id;
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
</aside>
<script type="text/javascript">
    secs = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        document.cookie = 'timer='+timer;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
        }
        else {
            document.getElementById(id).style.display = 'none';
            alert('Your Time Is Up!');
            location.reload();
        }
    }
</script>
<?php require('footer.php'); ?>