<?php 
    require('header.php');
    if(isset($_POST['policy']) || isset($_SESSION['policy'])){
        $_SESSION['policy']=1;
    }
    else{
        echo "<script>window.location.href='sets'</script>";
        die();
    }
    if(isset($_COOKIE['timer']) and $_COOKIE['timer']!='deleted'){
        $_SESSION['timer']=$_COOKIE['timer'];
        unset($_COOKIE['timer']);
        if($_SESSION['timer']<1){
            echo "<script>window.location.href=\"finish\";</script>";
            die();
        }
    }
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
            <div id="timer" style="text-align:center;font-weight:bold;">
                Time Left : <span id="countdown-1"><?php echo $_SESSION['timer']; ?></span>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($key==0){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <li class="<?php echo $class;?>"><a href="#<?php echo str_replace(" ","",$value); ?>" data-toggle="tab"><?php echo $_SESSION['subject_name'][$key]; ?></a></li>
                    <?php
                        }
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($key==0){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <div class="tab-pane <?php echo $class; ?>" id="<?php echo str_replace(" ","",$value);; ?>">
                        <section class="col-lg-9" style="min-height:500px">
                            <div class="box box-primary">
                                <div class="box-body">
                                <?php
                                    $var = 'q'.$value;
                                ?>
                                    <ol>
                                        <li style="color:#F00;font-weight:bold;">Please do not refresh the page or press the back button from here on</li>
                                        <li>The clock has been set on the server and count down timer at the top right corner of the screen will display time left to complete the examination and  you can monitor the time you have to complete the exam.</li>
                                        <li>If the server is slow in responding then please do not worry, your time will not be lost.</li>
                                        <li>Click one of the answer option buttons to select your answer.</li>
                                        <li>To change an answer, simply click the desired option and choose your button.</li>
                                        <li>Click on <b>RESET</b> button to deselect a chosen answer.</li>
                                        <li>Click on  <b>SAVE &amp; NEXT</b> to save the answer before moving to the next question. The next question will automatically be displayed.</li>
                                        <li>Click on <b>SKIP</b> to move to the next question without saving the current question.</li>
                                        <li>Click on <b>MARK FOR REVIEW</b> to review your answer at later stage.</li>
                                        <li>Make sure you click on <b>SAVE &amp; NEXT</b> button everytime you want to save your answer.</li>
                                        <li>Until and unless you have clicked on the Save &amp; Next button, your answer will not be saved.</li>
                                        <li>To go to a question, click on the question number on the right side of the screen.</li>
                                        <li>The color coded diagram on the right side of the screen shows the status of the questions : 
                                            <table>
                                                <tr>
                                                    <td><div id="qbox" class="white">5</div></td>
                                                    <td>White - You have not attempted question number 5</td>
                                                </tr>
                                                <tr>
                                                    <td><div id="qbox" class="red">5</div></td>
                                                    <td>Red - You have visited but not answered question number 5</td>
                                                </tr>
                                                <tr>
                                                    <td><div id="qbox" class="green">5</div></td>
                                                    <td>Green - You have answered question number 5</td>
                                                </tr>
                                                <tr>
                                                    <td><div id="qbox" class="purple">5</div></td>
                                                    <td>Purple  - You have marked question number 9 for review</td>
                                                </tr>
                                            </table>
                                        </li>
                                        <li>All the answered questions(saved or marked) will be considered for calculating the final score.</li>
                                        <li style="color:#F00;font-weight:bold;">Do Not CLICK on the FINISH Button unless you have completed the exam.In case you click FINISH button, you will not be permitted to continue.</li>
                                    </ol>
                                </div>
                            </div>
                        </section>
                        <section class="col-lg-3">
                            <div class="box box-danger" >
                                <div class="box-body">
                                    <div id="jump" style="overflow-y:scroll">
                                    <?php
                                        $count=count($_SESSION[$var]['qid']);
                                        for($i=1;$i<=$count;$i++){
                                            $j=$i-1;
                                            if($_SESSION[$var]['ans'][$j]==0){
                                                $class="white";//Not Attempted
                                            }
                                            if($_SESSION[$var]['ans'][$j]==6 || $_SESSION[$var]['ans'][$j]==-1){
                                                $class="red";//Skipped
                                            }
                                            if($_SESSION[$var]['ans'][$j]!=-1 && $_SESSION[$var]['ans'][$j]!=6 &&$_SESSION[$var]['ans'][$j]!=0){
                                                $class="green";// Answered
                                            }
                                            if($_SESSION[$var]['mfr'][$j]==1 && $_SESSION[$var]['ans'][$j]!=6 && $_SESSION[$var]['ans'][$j]!=0){
                                                $class="purple";//Marked For Review
                                            }
                                    ?>
                                        <a href="exam?id=<?php echo $i; ?>&amp;sid=<?php echo $key ?>"><div id="qbox" class="<?php echo $class ?>"><?php echo $_SESSION[$var]['qno'][$i-1] ?></div></a>
                                    <?php 
                                        }
                                    ?>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary pull-left bte">Back To Exam</button>
                                    <button type="button" class="btn btn-primary pull-right saq">See All Questions</button>
                                    <div class="clearfix"></div>
                                    <p align="center" style="margin-top:10px"><button type="button" class="btn btn-primary finish">Finish</button></p>
                                </div>
                            </div>
                        </section>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
</aside>
<script type="text/javascript">
    secs = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        document.cookie = 'timer='+timer;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
        }
        else {
            document.getElementById(id).style.display = 'none';
            alert('Your Time Is Up!');
            location.reload();
        }
    }
</script>
<?php require('footer.php'); ?>