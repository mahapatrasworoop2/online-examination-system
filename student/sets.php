<?php 
    require('header.php');
    reset_session();
    if(isset($_GET['id'])){
	    $id=base64_decode($_GET['id']);
	    $sql="select pos,neg from oe_patterns where id=:id";
	    $stmt=$con->prepare($sql);
	    $stmt->execute(array(
	        'id' => $id
	    ));
	    if($row=$stmt->fetch()){
	        $_SESSION['pos']=$row['pos'];//Getting Positive and Negative Marks from Patterns
	        $_SESSION['neg']=$row['neg'];
	    }
		else{
			echo "<script>window.location.href='../student/'</script>";
	    	die();
	    }
	    $sql="select * from oe_sets where status=:status and pattern_id=:id";
	    $stmt=$con->prepare($sql);
	    $stmt->execute(array(
	    	'status' => 'X01',
	        'id' => $id
	    ));
	}
	else{
		echo "<script>window.location.href='../student/'</script>";
		die();
	}
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
        	<div class="row">
	        	<?php
	                while($row=$stmt->fetch()){
	            ?>
	            <div class="col-lg-3 col-xs-6">
	                <div class="small-box bg-aqua">
	                    <div class="inner">
	                        <h3><?php echo $row['set_name']; ?></h3>
	                    </div>
	                    <div class="icon">
	                        <i class="ion ion-android-timer" style="font-size:64px"></i>
	                    </div>
	                    <a href="exam-set?id=<?php echo base64_encode($row['id']) ?>" class="small-box-footer">
	                        Take Exam <i class="fa fa-arrow-circle-right"></i>
	                    </a>
	                </div>
	            </div>
	            <?php 
	                }
	            ?>
        	</div>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>