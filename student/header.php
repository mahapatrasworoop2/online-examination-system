<?php
    require('../lib/functions.php');
    $validate=new Validators();
    $validate->validate_student('../../online-examination-system/');
    $con=dbConnect();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?> | Student</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="shortcut icon" href="../images/icons/favicon.ico?v=2">
        <link href="../css/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap/ionicons.min.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../css/bootstrap/AdminLTE.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="skin-blue">
        <noscript>
            <div>Please Enable JavaScript Or Get A Better Browser To Use This Site</div>
        </noscript>
        <header class="header">
            <a href="../student/" class="logo">                
                <?php echo TITLE; ?>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span><?php echo $_SESSION['fname']." ".$_SESSION['lname']; ?> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header bg-light-blue">
                                    <?php
                                        if($_SESSION['pic']==""){
                                            $img='../images/users/user.png';
                                    ?>
                                    <?php
                                        }
                                        else{
                                            $img="../images/users/".$_SESSION['pic'];
                                        }
                                    ?>
                                    <img src="<?php echo $img; ?>" class="img-circle" alt="User Image" />
                                    <p>
                                        <?php echo $_SESSION['fname']." ".$_SESSION['lname']; ?> - (Student)
                                        <small>Member Since <?php echo date('d F, Y', strtotime($_SESSION['registered'])); ?></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="../lib/logout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <aside class="left-side sidebar-offcanvas collapse-left">
                <section class="sidebar">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?php echo $img; ?>" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?php echo $_SESSION['fname']; ?></p>
                        </div>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="../student/">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="javascript:void()">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Patterns</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                            <?php
                                $sql="select * from oe_patterns where status=:status order by id asc";
                                $stmt=$con->prepare($sql);
                                $stmt->execute(array(
                                    'status' => 'X01'
                                ));
                                while($row=$stmt->fetch()){
                            ?>
                                <li><a href="sets?id=<?php echo base64_encode($row['id']); ?>"><i class="fa fa-angle-double-right"></i><?php echo $row['pattern_name']; ?></a></li>
                            <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </section>
            </aside>