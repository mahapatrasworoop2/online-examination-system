        </div>
        <!-- End of Wrapper -->
        <script src="../js/jquery.min.js"></script>
        <script src="../js/bootstrap/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap/plugins/iCheck/icheck.min.js" type="text/javascript"></script>
        <script src="../js/bootstrap/AdminLTE/app.js" type="text/javascript"></script>
        <script src="../js/bootstrap/AdminLTE/dashboard.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $('.mfr').click(function(){
                    var sid = $(this).attr('aria-data');                    
                    $('#action'+sid).val('mfr');
                    $('#frmExam'+sid).submit();
                });
                $('.skip').click(function(){
                    var sid = $(this).attr('aria-data');
                    $('#action'+sid).val('skip');
                    $('#frmExam'+sid).submit();
                });
                $('.sn').click(function(){
                    var sid = $(this).attr('aria-data');
                    $('#action'+sid).val('sn');
                    $('#frmExam'+sid).submit();
                });
                $('.reset').click(function(){
                    $("input[type='radio']").iCheck('uncheck');
                });
                $('.finish').click(function(){
                    var okk=confirm('Are You Sure You Want To Finish The Exam?');
                    if(okk){
                        window.location.href="finish?id=finish";
                    }
                });
                $('.bte').click(function(){
                    window.location.href = 'exam?id=1&sid=0';
                });
                $('.gtd').click(function(){
                    window.location.href = 'sets';
                });
                $('.saq').click(function(){
                    window.location.href = 'saq';
                });
                $('.inst').click(function(){
                    window.location.href = 'inst';
                });
            });
        </script>
    </body>
</html>
<?php
    if(isset($stmt)){
        $stmt=null;
        $con=null;
    }
?>