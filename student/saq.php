<?php 
    require('header.php');
    if(isset($_POST['policy']) || isset($_SESSION['policy'])){
        $_SESSION['policy']=1;
    }
    else{
        echo "<script>window.location.href='sets'</script>";
        die();
    }
    if(isset($_COOKIE['timer']) and $_COOKIE['timer']!='deleted'){
        $_SESSION['timer']=$_COOKIE['timer'];
        unset($_COOKIE['timer']);
        if($_SESSION['timer']<1){
            echo "<script>window.location.href=\"finish\";</script>";
            die();
        }
    }
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
            <div id="timer" style="text-align:center;font-weight:bold;">
                Time Left : <span id="countdown-1"><?php echo $_SESSION['timer']; ?></span>
            </div>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($key==0){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <li class="<?php echo $class;?>"><a href="#<?php echo str_replace(" ","",$value); ?>" data-toggle="tab"><?php echo $_SESSION['subject_name'][$key]; ?></a></li>
                    <?php
                        }
                    ?>
                </ul>
                <div class="tab-content">
                    <?php
                        foreach ($_SESSION['subject_id'] as $key => $value) {
                            if($key==0){
                                $class="active";
                            }
                            else{
                                $class="";
                            }
                    ?>
                    <div class="tab-pane <?php echo $class; ?>" id="<?php echo str_replace(" ","",$value);; ?>">
                        <section class="col-lg-9" style="min-height:500px">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <table class="table table-bordered table-hover">
                                <?php
                                    $var = 'q'.$value;
                                    $sql="select * from oe_questions where subject_id=:subject_id and set_id=:set_id";
                                    try{
                                        $stmt=$con->prepare($sql);
                                        $stmt->execute(array(
                                            'subject_id' => $value,
                                            'set_id' => $_SESSION['set_id']
                                        ));
                                        $id=0;
                                        while($row=$stmt->fetch()){
                                            $id++;
                                            $question=trim($row['question']);
                                            $option1=trim($row['option1']);
                                            $option2=trim($row['option2']);
                                            $option3=trim($row['option3']);
                                            $option4=trim($row['option4']);
                                            $option5=trim($row['option5']);
                                            $directions=trim($row['directions']);
                                        
                                ?>
                                        <tr>
                                            <td colspan="2"><strong><?php echo $directions; ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td style="width:50px"><strong><?php echo $_SESSION[$var]['qno'][$id-1] ?>.</strong></td>
                                            <td><?php echo $question; ?></td>
                                        </tr>
                                <?php
                                        }
                                    }
                                    catch(PDOException $error) {
                                        echo DBERROR . $error->getMessage();
                                        die();
                                    }
                                ?>
                                    </table>
                                </div>
                            </div>
                        </section>
                        <section class="col-lg-3">
                            <div class="box box-danger" >
                                <div class="box-body">
                                    <div id="jump" style="overflow-y:scroll">
                                    <?php
                                        $count=count($_SESSION[$var]['qid']);
                                        for($i=1;$i<=$count;$i++){
                                            $j=$i-1;
                                            if($_SESSION[$var]['ans'][$j]==0){
                                                $class="white";//Not Attempted
                                            }
                                            if($_SESSION[$var]['ans'][$j]==6 || $_SESSION[$var]['ans'][$j]==-1){
                                                $class="red";//Skipped
                                            }
                                            if($_SESSION[$var]['ans'][$j]!=-1 && $_SESSION[$var]['ans'][$j]!=6 &&$_SESSION[$var]['ans'][$j]!=0){
                                                $class="green";// Answered
                                            }
                                            if($_SESSION[$var]['mfr'][$j]==1 && $_SESSION[$var]['ans'][$j]!=6 && $_SESSION[$var]['ans'][$j]!=0){
                                                $class="purple";//Marked For Review
                                            }
                                    ?>
                                        <a href="exam?id=<?php echo $i; ?>&amp;sid=<?php echo $key ?>"><div id="qbox" class="<?php echo $class ?>"><?php echo $_SESSION[$var]['qno'][$i-1] ?></div></a>
                                    <?php 
                                        }
                                    ?>
                                    </div>
                                </div>
                                <div class="box-footer">
                                    <button type="button" class="btn btn-primary pull-left bte">Back To Exam</button>
                                    <button type="button" class="btn btn-primary pull-right inst">View Instructions</button>
                                    <div class="clearfix"></div>
                                    <p align="center" style="margin-top:10px"><button type="button" class="btn btn-primary finish">Finish</button></p>
                                </div>
                            </div>
                        </section>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
</aside>
<script type="text/javascript">
    secs = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        document.cookie = 'timer='+timer;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
        }
        else {
            document.getElementById(id).style.display = 'none';
            alert('Your Time Is Up!');
            location.reload();
        }
    }
</script>
<?php require('footer.php'); ?>