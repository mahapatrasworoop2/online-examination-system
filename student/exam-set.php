<?php 
    require('header.php');
    if(isset($_GET['id'])){
    	if(isset($_SESSION['set_id'])){
    		echo "<script>window.location.href='../student/'</script>";
			die();
    	}
    	$id=base64_decode($_GET['id']);
    	$_SESSION['set_id']=$id;
    	try{
	    	$sql="select * from oe_sets where id=:id";
		    $stmt=$con->prepare($sql);
		    $stmt->execute(array(
		        'id' => $id
		    ));
		    if($row=$stmt->fetch()){
		    	$_SESSION['set_name']=$row['set_name'];
	            $_SESSION['timer']=$row['timer'];
	            $sql="select distinct subject_id from oe_questions where status=:status and set_id=:id";
		        $stmt=$con->prepare($sql);
			    $stmt->execute(array(
			        'status' => 'X01',
			        'id' => $id
			    ));
			    while($row=$stmt->fetch()){
			        $_SESSION['subject_id'][]=$row['subject_id'];
			        $sql="select subject_name from oe_subjects where id=:subject_id";
			        $stmt_subject=$con->prepare($sql);
				    $stmt_subject->execute(array(
				        'subject_id' => $row['subject_id']
				    ));
				    while($row_subject=$stmt_subject->fetch()){
				    	$_SESSION['subject_name'][]=$row_subject['subject_name'];
				    }
			    }
			    $qno=0;
			    foreach ($_SESSION['subject_id'] as $key => $value) {
			        $sql="select id,ans from oe_questions where status=:status and set_id=:set_id and subject_id=:subject_id";
			        $stmt=$con->prepare($sql);
					$stmt->execute(array(
						'subject_id' => $value,
						'set_id' => $id,
						'status' => 'X01'
					));
					$var = 'q'.$value;
					while($row=$stmt->fetch()){
						$_SESSION[$var]['qid'][]=$row['id'];
						$_SESSION[$var]['correct'][]=$row['ans'];
				        $_SESSION[$var]['ans'][]=0;
				        $_SESSION[$var]['mfr'][]=0;
				        $qno++;
				        $_SESSION[$var]['qno'][]=$qno;
					}
			    }
			}
		    else{
			    echo "<script>window.location.href='../student/'</script>";
				die();
			}
		}
		catch(PDOException $error) {
			echo DBERROR . $error->getMessage();
			die();
		}
    }
    else{
    	echo "<script>window.location.href='../student/'</script>";
		die();
    }
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
        	<section class="col-lg-12">
                <div class="box box-primary">
                	<ol>
                        <li style="color:#F00;font-weight:bold;">Please do not refresh the page or press the back button from here on</li>
                        <li>The clock has been set on the server and count down timer at the top right corner of the screen will display time left to complete the examination and  you can monitor the time you have to complete the exam.</li>
                        <li>If the server is slow in responding then please do not worry, your time will not be lost.</li>
                        <li>Click one of the answer option buttons to select your answer.</li>
                        <li>To change an answer, simply click the desired option and choose your button.</li>
                        <li>Click on <b>RESET</b> button to deselect a chosen answer.</li>
                        <li>Click on  <b>SAVE &amp; NEXT</b> to save the answer before moving to the next question. The next question will automatically be displayed.</li>
                        <li>Click on <b>SKIP</b> to move to the next question without saving the current question.</li>
                        <li>Click on <b>MARK FOR REVIEW</b> to review your answer at later stage.</li>
                        <li>Make sure you click on <b>SAVE &amp; NEXT</b> button everytime you want to save your answer.</li>
                        <li>Until and unless you have clicked on the Save &amp; Next button, your answer will not be saved.</li>
                        <li>To go to a question, click on the question number on the right side of the screen.</li>
                        <li>The color coded diagram on the right side of the screen shows the status of the questions : 
                            <table>
                                <tr>
                                    <td><div id="qbox" class="white">5</div></td>
                                    <td>White - You have not attempted question number 5</td>
                                </tr>
                                <tr>
                                    <td><div id="qbox" class="red">5</div></td>
                                    <td>Red - You have visited but not answered question number 5</td>
                                </tr>
                                <tr>
                                    <td><div id="qbox" class="green">5</div></td>
                                    <td>Green - You have answered question number 5</td>
                                </tr>
                                <tr>
                                    <td><div id="qbox" class="purple">5</div></td>
                                    <td>Purple  - You have marked question number 9 for review</td>
                                </tr>
                            </table>
                        </li>
                        <li>All the answered questions(saved or marked) will be considered for calculating the final score.</li>
                        <li style="color:#F00;font-weight:bold;">Do Not CLICK on the FINISH Button unless you have completed the exam.In case you click FINISH button, you will not be permitted to continue.</li>
                    </ol>
                    <form name="frmAccept" action="exam?id=1&amp;sid=0" method="post">
                        <p style="padding-left:17px">
                            <input type="checkbox" name="policy" value="1" class="form-control">&nbsp;&nbsp;I have read and understood the instructions. I agree that in case of not adhering to the examination instructions, I will be disqualified from giving the examination.<br>
                            <button type="submit" class="btn btn-primary" style="margin-top:10px;margin-bottom:10px">Start Examination</button>
                        </p>
                    </form>
                </div>
            </section>
        </div>
    </section>
</aside>
<?php require('footer.php'); ?>