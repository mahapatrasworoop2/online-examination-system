<?php 
    require('header.php');
    if(isset($_POST['policy']) || isset($_SESSION['policy'])){
        $_SESSION['policy']=1;
    }
    else{
        echo "<script>window.location.href='sets'</script>";
        die();
    }
    if(isset($_COOKIE['timer']) and $_COOKIE['timer']!='deleted'){
        $_SESSION['timer']=$_COOKIE['timer'];
        unset($_COOKIE['timer']);
    }
    if(isset($_GET['id'])){
        if($_GET['id']=='finish'){
            $_SESSION['timer']=0;
        }
    }
?>
<aside class="right-side strech">
    <section class="content">
        <div class="row">
            <div id="timer" style="text-align:center;font-weight:bold;">
                Time Left : <span id="countdown-1"><?php echo $_SESSION['timer']; ?></span>
            </div>
            <section class="col-lg-9" style="min-height:500px">
                <div class="box box-primary">
                    <div class="box-body">
                    <?php
                        if($_SESSION['timer']>0){
                    ?>
                        <ol>
                            <li>Please click finish button or wait for your time to finish to see your result.</li>
                            <li>You may go back to the exam by clicking Back To Exam button.</li>
                        </ol>
                    <?php
                        }
                        else{
                            $not_visited=0;
                            $skipped=0;
                            $marked_for_review=0;
                            $correct=0;
                            $wrong=0;
                            $totalquestions=0;
                            foreach ($_SESSION['subject_id'] as $key => $value) {
                                $var = 'q'.$value;
                                foreach ($_SESSION[$var]['qid'] as $qkey => $qvalue) {
                                    if($_SESSION[$var]['mfr'][$qkey]==1){
                                        $marked_for_review++;
                                    }
                                    if($_SESSION[$var]['ans'][$qkey]==0){
                                        $not_visited++;
                                    }
                                    else if($_SESSION[$var]['ans'][$qkey]==6){
                                        $skipped++;
                                    }
                                    else if($_SESSION[$var]['ans'][$qkey]==$_SESSION[$var]['correct'][$qkey]){
                                        $correct++;
                                    }
                                    else if($_SESSION[$var]['ans'][$qkey]!=-1){
                                        $wrong++;
                                    }
                                }
                                $totalquestions+=count($_SESSION[$var]['qid']);
                            }
                            $marks=($correct*$_SESSION['pos'])-($wrong*$_SESSION['neg']);
                            if(!isset($_SESSION['inserted_into_database'])){
                                $sql="insert into oe_results(user_login,set_id,exam_taken_on,correct,incorrect,not_visited,not_answered) values(:user_login,:set_id,:exam_taken_on,:correct,:incorrect,:not_visited,:not_answered)";
                                $stmt=$con->prepare($sql);
                                $stmt->execute(array(
                                    'user_login' => $_SESSION['username'],
                                    'set_id' => $_SESSION['set_id'],
                                    'exam_taken_on' => date("Y-m-d h:i:s"),
                                    'correct' => $correct,
                                    'incorrect' => $wrong,
                                    'not_visited' => $not_visited,
                                    'not_answered' => $skipped
                                ));
                                $_SESSION['inserted_into_database']=1;
                            }
                    ?>
                        <ul>
                            <li>Total Number of Questions : <?php echo $totalquestions; ?></li>
                            <li>Questions Not Visited : <?php echo $not_visited ?></li>
                            <li>Questions Skipped : <?php echo $skipped ?></li>
                            <li>Questions Marked For Review : <?php echo $marked_for_review ?></li>
                            <li>Correct : <?php echo $correct ?></li>
                            <li>Wrong : <?php echo $wrong ?></li>
                            <li>Marks Obtained : <?php echo $marks ?></li>
                        </ul>
                    <?php
                        }
                    ?>
                    </div>
                    <div class="box-footer">
                        <?php
                            if($_SESSION['timer']>0){
                        ?>
                        <button type="button" class="btn btn-primary bte">Back To Exam</button>
                        <button type="button" class="btn btn-primary finish">Finish</button>
                        <?php
                            }
                            else{
                        ?>
                        <button type="button" class="btn btn-primary gtd">Goto Dashboard</button>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </section>
        </div>
    </section>
</aside>
<script type="text/javascript">
    secs = parseInt(document.getElementById('countdown-1').innerHTML,10);
    setTimeout("countdown('countdown-1',"+secs+")", 1000);
    function countdown(id, timer){
        timer--;
        minRemain  = Math.floor(timer / 60);
        secsRemain = new String(timer - (minRemain * 60));
        if (secsRemain.length < 2) {
            secsRemain = '0' + secsRemain;
        }
        clock = minRemain + ":" + secsRemain;
        document.getElementById(id).innerHTML = clock;
        document.cookie = 'timer='+timer;
        if ( timer > 0 ) {
            setTimeout("countdown('" + id + "'," + timer + ")", 1000);
        }
        else {
            document.getElementById(id).style.display = 'none';
        }
    }
</script>
<?php require('footer.php'); ?>